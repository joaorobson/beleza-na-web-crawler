import csv

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
import re
import json
import time
import logging

MAIN_CATEGORIES = [
    "cabelos",
    "perfumes",
    "maquiagem",
    "cuidados-para-pele",
    "corpo-e-banho",
    "bem-estar-e-saude",
]

CSV_HEADER = [
    "category_1",
    "category_2",
    "category_3",
    "name",
    "brand",
    "price",
    "description",
    "image_link",
]


class Crawler:
    def __init__(self, driver, url: str) -> None:
        self.driver = driver
        self.main_url = url
        logging.basicConfig(level=logging.INFO)
        self.logger = logging.getLogger(__class__.__name__)

    def get_second_level_categories_links(self, soup):
        links: list = []
        sidebar = soup.find("aside")
        if sidebar:
            try:
                links = sidebar.findChildren("ul", recursive=False)[0].find("li").findAll("a")
            except:
                self.logger.error("Error trying to extract")

        return links

    def get_third_level_categories_links(self, soup):
        links: list = []
        sidebar = soup.find("aside")
        if sidebar:
            try:
                links = (
                    sidebar.findChildren("ul", recursive=False)[0]
                    .find("li")
                    .find("div")
                    .find("ul")
                    .find("li")
                    .findAll("a")
                )
            except:
                self.logger.error("Error trying to extract")

        return links

    def get_all_products(self):
        soup = BeautifulSoup(self.driver.page_source, features="html.parser")
        products_divs: list = []

        try:
            products_divs = soup.find("div", {"class": "showcase-gondola"}).findChildren(
                "div", recursive=False
            )
        except:
            self.logger.error("Error trying to collect all products")

        return products_divs

    def get_product_brand(self, product):
        brand: str = ""
        try:
            brand = product.find("span", {"class": "showcase-item-brand"}).find("strong").text
        except Exception as e:
            self.logger.error(f"Error trying extracting product brand: {e}")

        return brand

    def get_product_name(self, product):
        name: str = ""
        try:
            name = product.find("a", {"class": "showcase-item-title"}).text.strip()
        except Exception as e:
            self.logger.error(f"Error trying extracting product name: {e}")

        return name

    def get_product_description(self, product):
        description: str = ""

        try:
            description = product.find("p", {"class": "showcase-item-description"}).text.strip()
        except Exception as e:
            self.logger.error(f"Error trying extracting product description: {e}")

        return description

    def get_product_image_src(self, product):
        src: str = ""

        try:
            src = product.find("img").get("data-src")
        except Exception as e:
            self.logger.error(f"Error trying extracting product's image src: {e}")

        return src

    def get_product_price(self, product):
        price: str = ""

        try:
            price = product.find("span", {"class": "price-value"}).text
        except Exception as e:
            self.logger.error(f"Error trying extracting product price: {e}")

        return price

    def load_all_products(self):
        try:
            elem = self.driver.find_element(
                By.XPATH, '//button[contains(text(), "Carregar mais produtos")]'
            )
        except Exception as e:
            self.logger.error(f"Unable to load more products: {e}")
            elem = None

        while elem:
            elem.click()
            time.sleep(3)
            try:
                elem = self.driver.find_element(
                    By.XPATH, '//button[contains(text(), "Carregar mais produtos")]'
                )
            except Exception as e:
                self.logger.error(f"Unable to load more products: {e}")
                elem = None

    def extract_products_info(self, url: str, categories: list) -> list:
        self.logger.info(f"Extracting products from: {url}")
        products_list: list = []
        if url:
            self.driver.get(url)
            time.sleep(8)
            self.load_all_products()

            products = self.get_all_products()

            for product in products:
                name = self.get_product_name(product)
                brand = self.get_product_brand(product)
                price = self.get_product_price(product)
                description = self.get_product_description(product)
                image_src = self.get_product_image_src(product)
                products_list.append(
                    [
                        categories[0],
                        categories[1],
                        categories[2],
                        name,
                        brand,
                        price,
                        description,
                        image_src,
                    ]
                )
        return products_list

    def store_products_list(self, products_list, categories):
        with open(f"produtos.csv", "a", encoding="UTF8", newline="") as f:
            writer = csv.writer(f)

            # writer.writerow(CSV_HEADER)

            writer.writerows(products_list)

    def run(self):
        for main_category in MAIN_CATEGORIES:
            self.driver.get(f"{self.main_url}/{main_category}/")
            time.sleep(4)

            soup = BeautifulSoup(self.driver.page_source, features="html.parser")
            second_level_links = self.get_second_level_categories_links(soup)

            for link in second_level_links:
                self.logger.info(f"Extracting links from {link.get('href')}")
                self.driver.get(link.get("href"))
                time.sleep(8)
                soup = BeautifulSoup(self.driver.page_source, features="html.parser")
                third_level_links = self.get_third_level_categories_links(soup)

                # If father link has sublinks, ignore the father link
                if len(third_level_links) > 1:
                    third_level_links = third_level_links[1:]

                for inner_link in third_level_links:
                    url = inner_link.get("href")
                    categories = url.replace(f"{self.main_url}/", "").strip("/").split("/")

                    while len(categories) < 3:
                        categories.append("")

                    products_list = self.extract_products_info(url, categories)
                    self.store_products_list(products_list, categories)
        self.driver.close()


if __name__ == "__main__":
    options = Options()
    # options.add_argument("--headless")
    driver = webdriver.Firefox(options=options)
    crawler = Crawler(driver, "https://www.belezanaweb.com.br")
    crawler.run()
