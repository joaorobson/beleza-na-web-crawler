# "Beleza na web" crawler

## Requirements

Before run the crawler, install the Python dependencies:

```
pip install -r requirements.txt
```

> NOTE: It is necessary to have [Firefox](https://www.mozilla.org/en-US/firefox/new/) installed too.
For linux users, it can be done via CLI and using the respective package manager. Check the [documentation](https://support.mozilla.org/en-US/kb/install-firefox-linux)
for more information.

## Running the crawler

To run the crawler, execute:

```
python3 crawler.py
```
